<?php declare(strict_types=1);

namespace NPM\ServiceWebhookHandler\Tests\Handlers;

use Http\Mock\Client;
use NPM\ServiceWebhookHandler\Exceptions\ExtraValidationsFailedException;
use NPM\ServiceWebhookHandler\Exceptions\InvalidHeadersException;
use NPM\ServiceWebhookHandler\Handlers\GitHubHandler;
use NPM\ServiceWebhookHandler\Utils;
use Nyholm\Psr7\Response;
use PHPUnit\Framework\TestCase;

/**
 * Class GitHubHandlerTest
 *
 * @package NPM\ServiceWebhookHandler\Tests\Handlers
 */
class GitHubHandlerTest extends TestCase
{
    private static string $payload = '{}';

    protected function setUp(): void
    {
        $_SERVER['HTTP_X_GITHUB_DELIVERY'] = 'github-delivery';
        $_SERVER['HTTP_X_GITHUB_EVENT']    = 'github-event';
        $_SERVER['HTTP_X_HUB_SIGNATURE']   = 'sha1=f896fa42c720112e07141e6a27f5300ae6848423';

        // Mock the GitHub meta request.
        $_SERVER['REMOTE_ADDR'] = '1.2.3.4';
        $mock_client            = new Client();
        $mock_client->addResponse(new Response(200, [], '{"hooks": ["1.2.3.4/32"]}'));
        Utils::setClient($mock_client);

        parent::setUp();
    }

    /**
     * @dataProvider requiredHeaders
     */
    public function testInvalidHeaders(string $header): void
    {
        unset($_SERVER[$header]);

        $this->expectException(InvalidHeadersException::class);
        $this->expectExceptionMessage("Required Header '{$header}' is missing.");

        $handler = new GitHubHandler('github-s3cr3t');
        $handler->validate(self::$payload);
    }

    public function testGetHeaders(): void
    {
        $handler = new GitHubHandler('github-s3cr3t');
        $handler->validate(self::$payload);

        $this->assertSame('github-s3cr3t', $handler->getSecret());
        $this->assertSame('github-delivery', $handler->getDelivery());
        $this->assertSame('github-event', $handler->getEvent());
        $this->assertSame('sha1=f896fa42c720112e07141e6a27f5300ae6848423', $handler->getSignature());
    }

    public function testValidate(): void
    {
        $handler = new GitHubHandler('github-s3cr3t');
        $valid   = $handler->validate(self::$payload);

        $this->assertTrue($valid);
        $this->assertSame(json_decode(self::$payload, true), $handler->getData());
    }

    public function testExtraValidationFails(): void
    {
        $_SERVER['REMOTE_ADDR'] = '4.3.2.1';
        $this->expectException(ExtraValidationsFailedException::class);
        $this->expectExceptionMessage('IP validation failed.');

        $handler = new GitHubHandler('github-s3cr3t');
        $valid   = $handler->validate(self::$payload);

        $this->assertTrue($valid);
        $this->assertSame(json_decode(self::$payload, true), $handler->getData());
    }

    public function testValidateWithoutExtraValidations(): void
    {
        $handler = new GitHubHandler('github-s3cr3t', ['validate_ip' => false]);
        $valid   = $handler->validate(self::$payload);

        $this->assertTrue($valid);
        $this->assertSame(json_decode(self::$payload, true), $handler->getData());
    }

    /**
     * Data provider for testInvalidHeaders.
     */
    public function requiredHeaders(): array
    {
        return [
            ['HTTP_X_HUB_SIGNATURE'],
            ['HTTP_X_GITHUB_EVENT'],
            ['HTTP_X_GITHUB_DELIVERY'],
        ];
    }
}
