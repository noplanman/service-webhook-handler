<?php declare(strict_types=1);

namespace NPM\ServiceWebhookHandler\Tests\Handlers;

use NPM\ServiceWebhookHandler\Exceptions\InvalidHeadersException;
use NPM\ServiceWebhookHandler\Handlers\TravisCIHandler;
use PHPUnit\Framework\TestCase;

/**
 * Class TravisCIHandler
 *
 * @package NPM\ServiceWebhookHandler\Tests\Handlers
 */
class TravisCIHandlerTest extends TestCase
{
    protected function setUp(): void
    {
        $_SERVER['HTTP_SIGNATURE']        = '<signature>';
        $_SERVER['HTTP_TRAVIS_REPO_SLUG'] = 'repo/slug';

        parent::setUp();
    }

    /**
     * @dataProvider requiredHeaders
     */
    public function testInvalidHeaders(string $header): void
    {
        unset($_SERVER[$header]);

        $this->expectException(InvalidHeadersException::class);
        $this->expectExceptionMessage("Required Header '{$header}' is missing.");

        (new TravisCIHandler())->validate();
    }

    public function testGetHeaders(): void
    {
        $handler = new TravisCIHandler();
        $handler->validate();

        $this->assertSame('repo/slug', $handler->getRepoSlug());
        $this->assertSame('<signature>', $handler->getSignature());
    }

    public function testValidate(): void
    {
        $this->markTestSkipped('Cannot test without private key, so trust that it works =)');
    }

    /**
     * Data provider for testInvalidHeaders.
     */
    public function requiredHeaders(): array
    {
        return [
            ['HTTP_SIGNATURE'],
            ['HTTP_TRAVIS_REPO_SLUG'],
        ];
    }
}
