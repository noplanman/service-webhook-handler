<?php declare(strict_types=1);

namespace NPM\ServiceWebhookHandler\Tests\Handlers;

use NPM\ServiceWebhookHandler\Exceptions\InvalidHeadersException;
use NPM\ServiceWebhookHandler\Handlers\GitLabHandler;
use PHPUnit\Framework\TestCase;

/**
 * Class GitLabHandlerTest
 *
 * @package NPM\ServiceWebhookHandler\Tests\Handlers
 */
class GitLabHandlerTest extends TestCase
{
    private static string $payload = '{"secret":"gitlab-s3cr3t"}';

    protected function setUp(): void
    {
        $_SERVER['HTTP_X_GITLAB_TOKEN'] = 'gitlab-t0k3n';
        $_SERVER['HTTP_X_GITLAB_EVENT'] = 'gitlab-event';

        parent::setUp();
    }

    /**
     * @dataProvider requiredHeaders
     */
    public function testInvalidHeaders(string $header): void
    {
        unset($_SERVER[$header]);

        $this->expectException(InvalidHeadersException::class);
        $this->expectExceptionMessage("Required Header '{$header}' is missing.");

        $handler = new GitLabHandler('gitlab-s3cr3t');
        $handler->validate(self::$payload);
    }

    public function testGetHeaders(): void
    {
        $handler = new GitLabHandler('gitlab-s3cr3t');

        $handler->validate(self::$payload);

        $this->assertSame('gitlab-s3cr3t', $handler->getSecret());
        $this->assertSame('gitlab-t0k3n', $handler->getToken());
        $this->assertSame('gitlab-event', $handler->getEvent());
        $this->assertSame('', $handler->getSignature());
    }

    public function testValidateWithoutToken(): void
    {
        unset($_SERVER['HTTP_X_GITLAB_TOKEN']);
        $handler = new GitLabHandler('gitlab-s3cr3t');
        // $handler = new GitLabHandler();

        $valid = $handler->validate(self::$payload);

        $this->assertTrue($valid);
        $this->assertSame(json_decode(self::$payload, true), $handler->getData());
    }

    public function testValidateWithToken(): void
    {
        $handler = new GitLabHandler('gitlab-t0k3n');

        $valid = $handler->validate(self::$payload);

        $this->assertTrue($valid);
        $this->assertSame(json_decode(self::$payload, true), $handler->getData());
    }

    /**
     * Data provider for testInvalidHeaders.
     */
    public function requiredHeaders(): array
    {
        return [
            ['HTTP_X_GITLAB_EVENT'],
        ];
    }
}
