# Changelog
The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

Exclamation symbols (:exclamation:) note something of importance e.g. breaking changes. Click them to learn more.

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

## [1.0.0] - 2022-06-04
### Added
- PSR-16 caching layer.
- Library version to request header for cURL calls.
- Explicit error handling with Exceptions.
- Initial PHPUnit tests.
- PSR17 factory and PSR18 client implementation.
### Changed
- Moved repo to GitLab.
### Fixed
- Automatically fetch correct payload for Telegram Login.
### Security
- PHP 8.0+ required.

## [0.2.0] - 2018-03-08
### Added
- Telegram Login handler.
### Security
- Strict PHP 7.1+ required.

## [0.1.0] - 2017-04-13
### Added
- GitHub webhook handler.
- Travis-CI webhook handler.

[Unreleased]: https://github.com/noplanman/service-webhook-handler/compare/master...develop
[1.0.0]: https://gitlab.com/noplanman/service-webhook-handler/-/compare/0.2.0...1.0.0
[0.2.0]: https://gitlab.com/noplanman/service-webhook-handler/-/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/noplanman/service-webhook-handler/-/compare/299d0444984c455f6ec1f2ef61fde8ca72e1b6af...0.1.0
