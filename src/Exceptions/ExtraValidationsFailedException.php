<?php declare(strict_types=1);

namespace NPM\ServiceWebhookHandler\Exceptions;

class ExtraValidationsFailedException extends ServiceWebhookHandlerException
{

}
