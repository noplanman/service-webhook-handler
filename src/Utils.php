<?php declare(strict_types=1);

namespace NPM\ServiceWebhookHandler;

use Cache\Adapter\Void\VoidCachePool;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\Psr18ClientDiscovery;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

class Utils
{
    public const VERSION = '1.0.0';

    protected static ClientInterface $client;
    protected static CacheInterface $cache;

    public static function cidrMatch(string $ip, string $range): bool
    {
        [$subnet, $bits] = explode('/', $range);
        $ip     = ip2long($ip);
        $subnet = ip2long($subnet);
        $mask   = -1 << (32 - $bits);
        $subnet &= $mask; # nb: in case the supplied subnet wasn't correctly aligned

        return ($ip & $mask) === $subnet;
    }

    public static function fetchCacheableFile(string $url, string $key, int $cache_time = 60, ?string $default = null): ?string
    {
        try {
            self::$cache ??= new VoidCachePool();

            if ($data = self::$cache->get($key)) {
                return $data;
            }

            if (filter_var($url, FILTER_VALIDATE_URL)) {
                $client          = self::$client ?? Psr18ClientDiscovery::find();
                $request_factory = Psr17FactoryDiscovery::findRequestFactory();

                $response = $client->sendRequest(
                    $request_factory
                        ->createRequest('GET', $url)
                        ->withHeader('User-Agent', 'PHP-Service-Webhook-Handler v' . self::VERSION)
                );

                $data = (string) $response->getBody();

                self::$cache->set($key, $data, $cache_time);
            }

            return $data ?? $default;
        } catch (InvalidArgumentException|ClientExceptionInterface) {
            return $default;
        }
    }

    public static function setCache(CacheInterface $cache): void
    {
        self::$cache = $cache;
    }

    public static function setClient(ClientInterface $client): void
    {
        self::$client = $client;
    }
}
