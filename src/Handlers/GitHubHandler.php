<?php declare(strict_types=1);

namespace NPM\ServiceWebhookHandler\Handlers;

use NPM\ServiceWebhookHandler\Exceptions\ExtraValidationsFailedException;
use NPM\ServiceWebhookHandler\Utils;

/**
 * Class GitHubHandler
 *
 * @package NPM\ServiceWebhookHandler\Handlers
 */
class GitHubHandler extends WebhookHandler
{
    public const API_HOST            = 'https://api.github.com';
    public const API_META_CACHE_TIME = 60;

    protected array $options;
    protected string $event;
    protected string $delivery;

    public function __construct(
        protected string $secret,
        array $options = []
    ) {
        $this->options = array_merge([
            'validate_ip' => true,
        ], $options);
    }

    /**
     * Check if the incoming IP is coming from a GitHub hook.
     *
     * To do this, we use the GitHub API meta endpoint to get a list of valid IPs.
     *
     * @return bool
     * @throws ExtraValidationsFailedException
     */
    protected function validateIp(): bool
    {
        $meta = $this->getGitHubApiMeta();

        $ip     = $_SERVER['REMOTE_ADDR'] ?? null;
        $ranges = (array) ($meta['hooks'] ?? []);

        // Check if IP comes from a GitHub hook.
        foreach ($ranges as $range) {
            if (Utils::cidrMatch($ip, $range)) {
                return true;
            }
        }

        throw new ExtraValidationsFailedException('IP validation failed.');
    }

    protected function extraValidations(): bool
    {
        if ($this->options['validate_ip']) {
            return $this->validateIp();
        }

        return true;
    }

    public function getDelivery(): string
    {
        return $this->delivery;
    }

    public function getEvent(): string
    {
        return $this->event;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    protected function getVitalHeaders(): array
    {
        return [
            'signature' => 'HTTP_X_HUB_SIGNATURE',
            'event'     => 'HTTP_X_GITHUB_EVENT',
            'delivery'  => 'HTTP_X_GITHUB_DELIVERY',
        ];
    }

    protected function validateSignature(string $signature, string $payload): bool
    {
        [$algo, $real_signature] = explode('=', $signature);

        if ($algo !== 'sha1') {
            // see https://developer.github.com/webhooks/securing/
            return false;
        }

        $payload_hash = hash_hmac($algo, $payload, $this->secret);

        return hash_equals($real_signature, $payload_hash);
    }

    protected function getGitHubApiMeta(string $api_host = ''): array
    {
        if ($api_host === '') {
            $api_host = self::API_HOST;
        }

        $api_meta = Utils::fetchCacheableFile(
            $api_host . '/meta',
            'github_api_meta_json',
            self::API_META_CACHE_TIME
        ) ?: null;

        return json_decode($api_meta, true) ?: [];
    }
}
