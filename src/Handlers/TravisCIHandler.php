<?php declare(strict_types=1);

namespace NPM\ServiceWebhookHandler\Handlers;

use NPM\ServiceWebhookHandler\Utils;

/**
 * Class TravisCIHandler
 *
 * @package NPM\ServiceWebhookHandler\Handlers
 */
class TravisCIHandler extends WebhookHandler
{
    public const API_HOST              = 'https://api.travis-ci.org';
    public const API_CONFIG_CACHE_TIME = 60;

    protected string $repo_slug;

    public function getRepoSlug(): string
    {
        return $this->repo_slug;
    }

    protected function getVitalHeaders(): array
    {
        return [
            'signature' => 'HTTP_SIGNATURE',
            'repo_slug' => 'HTTP_TRAVIS_REPO_SLUG',
        ];
    }

    protected function validateSignature(string $signature, string $payload): bool
    {
        if ($pubkey = $this->getTravisPubKey()) {
            return openssl_verify($payload, base64_decode($signature), $pubkey) === 1;
        }

        return false;
    }

    protected function getTravisPubKey(): string
    {
        $api_config = $this->getTravisApiConfig();

        return $api_config['config']['notifications']['webhook']['public_key'] ?? '';
    }

    protected function getTravisApiConfig(string $api_host = ''): array
    {
        if ($api_host === '') {
            $api_host = self::API_HOST;
        }

        $api_config = Utils::fetchCacheableFile(
            $api_host . '/config',
            'travis_ci_api_config_json',
            self::API_CONFIG_CACHE_TIME
        ) ?: null;

        return json_decode($api_config, true) ?: [];
    }
}
