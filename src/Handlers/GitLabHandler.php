<?php declare(strict_types=1);

namespace NPM\ServiceWebhookHandler\Handlers;

/**
 * Class GitLabHandler
 *
 * @package NPM\ServiceWebhookHandler\Handlers
 */
class GitLabHandler extends WebhookHandler
{
    protected string $token;
    protected string $event;

    public function __construct(
        protected ?string $secret = null
    ) {
        // Required but not necessary for GitLab.
        $this->signature = '';
    }

    public function getEvent(): string
    {
        return $this->event;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    protected function getVitalHeaders(): array
    {
        $vital_headers = [
            'event' => 'HTTP_X_GITLAB_EVENT',
        ];

        // The secret could be left out, so only enforce the check if it's here.
        if (isset($_SERVER['HTTP_X_GITLAB_TOKEN'])) {
            $vital_headers['token'] = 'HTTP_X_GITLAB_TOKEN';
        }

        return $vital_headers;
    }

    protected function validateSignature(string $signature, string $payload): bool
    {
        return !isset($this->token) || $this->token === $this->secret;
    }
}
