<?php declare(strict_types=1);

namespace NPM\ServiceWebhookHandler\Handlers;

use NPM\ServiceWebhookHandler\Exceptions\ExtraValidationsFailedException;
use NPM\ServiceWebhookHandler\Exceptions\InvalidHeadersException;

/**
 * Class WebhookHandler
 *
 * @package NPM\ServiceWebhookHandler\Handlers
 */
abstract class WebhookHandler
{
    protected ?array $data;
    protected string $signature;

    public function getData(): ?array
    {
        return $this->data;
    }

    public function getSignature(): string
    {
        return $this->signature;
    }

    /**
     * Define a list of vital headers required for the validation.
     */
    abstract protected function getVitalHeaders(): array;

    /**
     * Validate payload with given signature, normally set via HTTP header.
     */
    abstract protected function validateSignature(string $signature, string $payload): bool;

    /**
     * Validate the vital headers and set member variables.
     *
     * @throws InvalidHeadersException
     */
    protected function validateHeaders(): void
    {
        foreach ($this->getVitalHeaders() as $key => $header) {
            if (($value = $_SERVER[$header] ?? null) === null) {
                throw new InvalidHeadersException("Required Header '{$header}' is missing.");
            }
            $this->$key = $value;
        }
    }

    protected function extraValidations(): bool
    {
        return true;
    }

    /**
     * @throws InvalidHeadersException
     * @throws ExtraValidationsFailedException
     */
    public function validate(string $payload = ''): bool
    {
        $this->validateHeaders();
        if (!$this->extraValidations()) {
            throw new ExtraValidationsFailedException('Extra validations failed.');
        }

        $payload = $this->loadPayload($payload);

        if (!$this->validateSignature($this->signature, $payload)) {
            return false;
        }

        $this->data = json_decode($payload, true);

        return true;
    }

    protected function loadPayload(string $payload = ''): string
    {
        $payload !== '' || $payload = (string) file_get_contents('php://input');

        // Check if the payload is json or urlencoded.
        if (str_starts_with($payload, 'payload=')) {
            $payload = substr(urldecode($payload), 8);
        }

        return $payload;
    }
}
