# [Service Webhook Handler][github-swh]

[![Minimum PHP Version][min-php-version-badge]][packagist-swh]
[![Latest Stable Version][latest-version-badge]][packagist-swh]
[![Total Downloads][total-downloads-badge]][packagist-swh]
[![License][license-badge]][license]

PHP library to handle Webhooks from various services.

## Requirements

* PHP >= 8.0
* Any [PSR-17 implementation](https://packagist.org/providers/psr/http-factory-implementation)
* Any [PSR-18 implementation](https://packagist.org/providers/psr/http-client-implementation)

## Quick install via [Composer]

This command will get you up and running quickly with a Guzzle HTTP client.

```bash
composer require noplanman/service-webhook-handler:^1.0 guzzlehttp/guzzle:^7.4.3 guzzlehttp/psr7:^2.2
```

### HTTP Client

You can explicitly set an HTTP client with:

```php
use NPM\ServiceWebhookHandler\Utils;

// Example with Guzzle ($ composer require guzzlehttp/guzzle:^7.4.3 guzzlehttp/psr7:^2.2)
use GuzzleHttp\Client;
Utils::setClient(new Client());
```

```php
use NPM\ServiceWebhookHandler\Utils;

// Example with HTTPlug with cURL ($ composer require php-http/curl-client:^2.0 nyholm/psr7:^1.1)
use Http\Client\Curl\Client;
Utils::setClient(new Client());
```

### Caching

To enable caching, add any [PSR-16 compatible adapter](https://packagist.org/providers/psr/simple-cache-implementation) and set the cache provider:

```php
use NPM\ServiceWebhookHandler\Utils;

// Example with redis ($ composer require cache/redis-adapter)
use Cache\Adapter\Redis\RedisCachePool;
$client = new Redis();
$client->connect('127.0.0.1', 6379);
Utils::setCache(new RedisCachePool($client));
```

```php
use NPM\ServiceWebhookHandler\Utils;

// Example with memcached ($ composer require cache/memcached-adapter)
use Cache\Adapter\Memcached\MemcachedCachePool;
$client = new Memcached();
$client->addServer('localhost', 11211);
Utils::setCache(new MemcachedCachePool($client));
```

## Usage

Very basic functionality provided so far for:

### GitHub

[Docs][github-webhook-docs] - [`GitHubHandler.php`][github-handler-php]

```php
use NPM\ServiceWebhookHandler\Handlers\GitHubHandler;

$handler = new GitHubHandler('webhook_secret');
if ($handler->validate()) {
    // All good, use the received data!
    $data = $handler->getData();
}
```

### GitLab

[Docs][gitlab-webhook-docs] - [`GitLabHandler.php`][gitlab-handler-php]

```php
use NPM\ServiceWebhookHandler\Handlers\GitLabHandler;

$handler = new GitLabHandler('webhook_secret');
if ($handler->validate()) {
    // All good, use the received data!
    $data = $handler->getData();
}
```

### Travis CI

[Docs][travis-ci-webhook-docs] - [`TravisCIHandler.php`][travis-ci-handler-php]

```php
use NPM\ServiceWebhookHandler\Handlers\TravisCIHandler;

$handler = new TravisCIHandler();
if ($handler->validate()) {
    // All good, use the received data!
    $data = $handler->getData();
}
```

### Telegram Login

[Docs][telegram-login-webhook-docs] - [`TelegramLoginHandler.php`][telegram-login-handler-php]

```php
use NPM\ServiceWebhookHandler\Handlers\TelegramLoginHandler;

$handler = new TelegramLoginHandler('123:BOT_API_KEY');
if ($handler->validate()) {
    // All good, use the received data!
    $data = $handler->getData();
}
```

[github-swh]: https://gitlab.com/noplanman/service-webhook-handler "Service Webhook Handler on GitHub"
[packagist-swh]: https://packagist.org/packages/noplanman/service-webhook-handler "Service Webhook Handler on Packagist"
[license]: https://gitlab.com/noplanman/service-webhook-handler/blob/master/LICENSE "Service Webhook Handler license"

[latest-version-badge]: https://img.shields.io/packagist/v/noplanman/service-webhook-handler.svg
[min-php-version-badge]: https://img.shields.io/packagist/php-v/noplanman/service-webhook-handler.svg
[total-downloads-badge]: https://img.shields.io/packagist/dt/noplanman/service-webhook-handler.svg
[license-badge]: https://img.shields.io/packagist/l/noplanman/service-webhook-handler.svg
[Composer]: https://getcomposer.org/ "Composer"

[github-webhook-docs]: https://developer.github.com/webhooks/
[github-handler-php]: https://gitlab.com/noplanman/service-webhook-handler/blob/master/src/Handlers/GitHubHandler.php
[gitlab-webhook-docs]: https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/project/integrations/webhooks.md
[gitlab-handler-php]: https://gitlab.com/noplanman/service-webhook-handler/blob/master/src/Handlers/GitLabHandler.php
[travis-ci-webhook-docs]: https://docs.travis-ci.com/user/notifications/#Configuring-webhook-notifications
[travis-ci-handler-php]: https://gitlab.com/noplanman/service-webhook-handler/blob/master/src/Handlers/TravisCIHandler.php
[telegram-login-webhook-docs]: https://core.telegram.org/widgets/login
[telegram-login-handler-php]: https://gitlab.com/noplanman/service-webhook-handler/blob/master/src/Handlers/TelegramLoginHandler.php
